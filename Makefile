generate:
	protoc \
		-I protos/ \
		-I third_party/grpc-gateway/ \
		-I third_party/googleapis/ \
		--go_out=plugins=grpc,paths=source_relative:./protos \
		protos/user.proto