package main

import (
	"context"
	"encoding/json"
	"log"
	"net"
	"net/http"

	md "go-assignment/helper"
	pb "go-assignment/protos"
	backend "go-assignment/server"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
)

func startGRPCServer(address string) {
	lis, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("Failed to listen :%v", err)
	}

	// create server instance
	server := grpc.NewServer()
	pb.RegisterUserServiceServer(server, &backend.UserService{})
	log.Printf("GRPC Server started at : %v", address)
	go func() {
		log.Fatal(server.Serve(lis))
	}()
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("content-type", "application/json")
	vars := mux.Vars(r)
	collection := md.ConnectDB("users")
	user := &pb.User{}
	objID, _ := primitive.ObjectIDFromHex(vars["user_id"])
	filter := bson.D{primitive.E{Key: "_id", Value: objID}}
	err := collection.FindOne(context.TODO(), filter).Decode(user)
	// log.Printf("User id : %v, Result : %v", vars["user_id"], result.Decode(user))
	// result.Decode(user)
	log.Printf("User from DB : %v", user)

	if err != nil {
		log.Printf("User id : %v not found with error : %v", vars["user_id"], err)
		http.Error(w, err.Error(), 404)
	} else {
		employee := &pb.Employee{}
		collection = md.ConnectDB("employees")
		filter = bson.D{primitive.E{Key: "userid", Value: vars["user_id"]}}
		err = collection.FindOne(context.TODO(), filter).Decode(employee)

		if err != nil {
			log.Printf("Employee not found with user id : %v and error : %v", vars["user_id"], err)
			http.Error(w, err.Error(), 404)
		} else {
			type ResponseBody struct {
				Firstname   string
				Lastname    string
				Email       string
				EmployeeID  string
				Designation string
			}
			log.Printf("Employee found : %v", employee)
			var rb ResponseBody
			rb = ResponseBody{
				Firstname:   user.Firstname,
				Lastname:    user.Lastname,
				Email:       user.Email,
				EmployeeID:  employee.Id,
				Designation: employee.Designation,
			}
			json.NewEncoder(w).Encode(rb)
		}
	}
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	collection := md.ConnectDB("users")
	// user := &pb.User{}
	objID, _ := primitive.ObjectIDFromHex(vars["user_id"])
	filter := bson.D{primitive.E{Key: "_id", Value: objID}}
	update := bson.D{
		{"$set", bson.D{
			primitive.E{Key: "email", Value: vars["email"]},
		}},
	}
	updatedResult, err := collection.UpdateOne(context.TODO(), filter, update)
	// log.Printf("User id : %v, Result : %v", vars["user_id"], result.Decode(user))
	// result.Decode(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Printf("Matched %v documents and updated %v documents.\n", updatedResult.MatchedCount, updatedResult.ModifiedCount)

	type Message struct {
		UpdatedCount int64
		Status       bool
	}
	w.Header().Set("Content-Type", "application/json")
	m := Message{}
	if updatedResult.MatchedCount == 0 {
		m = Message{
			UpdatedCount: updatedResult.ModifiedCount,
			Status:       false,
		}

	} else {
		m = Message{
			UpdatedCount: updatedResult.ModifiedCount,
			Status:       true,
		}
	}
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	json.NewEncoder(w).Encode(m)

}

func ListUsers(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("content-type", "application/json")
	collection := md.ConnectDB("users")
	var users []*pb.User
	findOptions := options.Find()

	cur, err := collection.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != nil {
		log.Fatal(err)
	}

	for cur.Next(context.TODO()) {
		var user pb.User
		err := cur.Decode(&user)
		if err != nil {
			log.Fatal(err)
		}

		users = append(users, &user)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	cur.Close(context.TODO())

	log.Printf("Found multiple documents (array of pointers): %+v\n", users)
	json.NewEncoder(w).Encode(users)
}

func AddUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("content-type", "application/json")

	collection := md.ConnectDB("users")
	type Body struct {
		Firstname   string
		Lastname    string
		Email       string
		Designation string
	}
	var data Body
	json.NewDecoder(r.Body).Decode(&data)
	log.Printf("Trying to add user : %v", data)

	user := &pb.User{
		Firstname: data.Firstname,
		Lastname:  data.Lastname,
		Email:     data.Email,
	}
	res, err := collection.InsertOne(context.Background(), user)
	if err != nil {
		log.Fatalf("Something went wrong while inserting document into mongodb..... : %v", err)
	}
	id := res.InsertedID
	log.Printf("Data inserted into Mongodb with id : %v", id)

	userId := id.(primitive.ObjectID).Hex()
	log.Printf("User ID : %v", userId)
	employee := &pb.Employee{
		Userid:      userId,
		Designation: data.Designation,
	}
	collection = md.ConnectDB("employees")
	result, error := collection.InsertOne(context.Background(), employee)
	if error != nil {
		log.Fatalf("Something went wrong while inserting document into mongodb..... : %v", err)
	}
	id = result.InsertedID
	log.Printf("Data inserted into Mongodb with id : %v", id)

	json.NewEncoder(w).Encode(res)
}

func main() {
	// Tried with both grpc server and rest server
	// address := "localhost:8000"
	// startGRPCServer(address)

	// REST server
	// ctx := context.Background()
	// ctx, cancel := context.WithCancel(ctx)
	// defer cancel()

	// required for grpc gateway
	// mux := runtime.NewServeMux()
	// opts := []grpc.DialOption{grpc.WithInsecure()}
	// if err := pb.RegisterUserServiceHandlerFromEndpoint(ctx, mux, address, opts); err != nil {
	// 	log.Fatalf("Error : %v\n", err)
	// }

	// implementing gorilla mux
	r := mux.NewRouter()
	r.HandleFunc("/assignment/user", AddUser).Methods("POST")
	r.HandleFunc("/assignment/user/{user_id}", GetUser).Methods("GET")
	r.HandleFunc("/assignment/user/{user_id}/{email}", UpdateUser).Methods("PATCH")
	r.HandleFunc("/assignment/users", ListUsers).Methods("GET")

	// r.ServeHTTP()
	log.Fatal(http.ListenAndServe(":8001", r))

}
