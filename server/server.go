package server

import (
	"context"
	md "go-assignment/helper"
	pb "go-assignment/protos"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserService struct{}

func (s *UserService) AddUser(ctx context.Context, in *pb.User) (*pb.AddUserResponse, error) {
	// connect mongo and get collection
	collection := md.ConnectDB("users")
	log.Printf("Request Body : %v\n", in)
	res, err := collection.InsertOne(context.Background(), in)
	if err != nil {
		log.Fatalf("Something went wrong while inserting document into mongodb..... : %v", err)
	}
	id := res.InsertedID
	log.Printf("Data inserted into Mongodb with id : %v", id)
	return &pb.AddUserResponse{User: in, Status: true}, nil

}

func (s *UserService) GetUser(ctx context.Context, in *pb.GetUserRequest) (*pb.User, error) {
	log.Printf("Retrieving user with userID : %v", in.GetUserId())
	// connect mongo and get collection
	collection := md.ConnectDB("users")
	result := collection.FindOne(context.TODO(), bson.M{"id": in.GetUserId()})
	user := &pb.User{}
	result.Decode(user)
	if user == nil {
		return nil, status.Errorf(codes.NotFound, "User %q not found", in.GetUserId())
	}
	// u = proto.Unmarshal(&user)
	return &pb.User{Firstname: user.Firstname, Lastname: user.Lastname, Email: user.Email}, nil
}
