# Go Assignment

## How to run?
> `$make generate`

## Libraries used
- Gorilla mux
- Protobuf 
- gRPC (optional)
- gRPC Gateway (optional)
- MongoDB
